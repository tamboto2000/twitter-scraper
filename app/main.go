package main

import (
	"application"
	"log"
	"net/http"
	"twitter_scraper/app/controller"
	"twitter_scraper/app/worker"

	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var app = new(application.App)
var ctrl = controller.Controllers()
var wkPool = worker.Pool()

func main() {
	//Conect to db
	panicOnError(app.AddDB("danu", "t%5pg&RdNqw*CBRxe7Vi1ah", "31.220.61.116", "3306", "data_lake_admin"))

	//migrate tables
	migrate(app.DB)
	// reMigrate(app.DB)

	//Create log
	log := log.NewLog(app.DB)

	//Create worker pool
	wkPool.Log = log

	//Assign log to controller
	ctrl.Log = log

	//Assign worker pool to controller
	ctrl.Worker = wkPool

	//Assign database to controller
	ctrl.DB = app.DB

	//Prepare app
	app.Router = Routes()
	app.Controller = ctrl
	app.Worker = wkPool
	app.Log = log

	http.ListenAndServe(":8000", app.Router.ExportRouter())
}

func panicOnError(err error) {
	if err != nil {
		panic(err.Error())
	}
}
