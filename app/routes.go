package main

import (
	"router"
)

var routes = router.NewRouter()

//Routes register routes
func Routes() *router.Router {
	routes.AddFunc("/scrape", ctrl.Controller("start_scrape"), "POST")
	routes.AddFunc("/download", ctrl.Controller("download"), "GET")
	return routes
}
