package models

import "time"

type Twitter struct {
	ID        int       `gorm:"AUTO_INCREMENT"`
	UserID    string    `gorm:"size:255"`
	RequestID string    `gorm:"size:255"`
	Type      string    `gorm:"size:255"`
	Data      string    `gorm:"type:longtext"`
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp" gorm:"default:current_timestamp"`
}
