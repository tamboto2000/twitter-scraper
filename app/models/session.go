package models

import "time"

type Session struct {
	ID        int       `gorm:"AUTO_INCREMENT"`
	UserID    string    `gorm:"size:255"`
	RequestID string    `gorm:"size:255"`
	Token     string    `gorm:"type:longtext"`
	CreatedAt time.Time `gorm:"type:timestamp" gorm:"default:current_timestamp"`
}
