package models

import "time"

type RetweetLocation struct {
	ID        int    `gorm:"AUTO_INCREMENT"`
	PostID    string `gorm:"size:255"`
	Long      float64
	Lat       float64
	CreatedAt time.Time `gorm:"type:timestamp" gorm:"default:current_timestamp"`
}
