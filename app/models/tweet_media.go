package models

import "time"

type TweetMedia struct {
	ID        int       `gorm:"AUTO_INCREMENT"`
	PostID    string    `gorm:"size:255"`
	URL       string    `gorm:"type:mediumtext"`
	Type      string    `gorm:"size:255"`
	CreatedAt time.Time `gorm:"type:timestamp" gorm:"default:current_timestamp"`
}
