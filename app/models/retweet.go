package models

import "time"

type Retweet struct {
	ID            int `gorm:"AUTO_INCREMENT"`
	User          string
	PostID        string `gorm:"size:255"`
	RetweetPostID string `gorm:"size:255"`
	RetweetOwner  string `gorm:"size:255"`
	Caption       string `gorm:"type:mediumtext"`
	Like          int
	RetweetCount  int
	CreatedAt     time.Time `gorm:"type:timestamp" gorm:"default:current_timestamp"`
}
