package controller

import (
	"bytes"
	"compress/gzip"
	"controller"
	"encoding/base64"
	"io/ioutil"
	"twitter_scraper/app/models"

	"github.com/jinzhu/gorm"
)

func download(ctx *controller.Context) {
	userID := ctx.Request.URL.Query().Get("userID")
	reqID := ctx.Request.URL.Query().Get("requestID")
	ty := ctx.Request.URL.Query().Get("type")

	if userID == "" || reqID == "" || ty == "" {
		ctx.BadRequest("userID, type, and requestID need to be filled")
		return
	}

	if ty != "json" && ty != "csv" {
		ctx.BadRequest("unrecognized value for parameter 'type'. Recognized value are: 'json', 'csv'")
		return
	}

	if ty == "csv" {
		data, err := loadCSV(userID, reqID, ctx.App.DB)
		if err != nil {
			if err.Error() == gorm.ErrRecordNotFound.Error() {
				ctx.NotFound("data might be not ready yet")
				return
			}

			ctx.InternalError(err.Error())
			return
		}

		ctx.ServeContent("result.zip", data)
	} else {
		data, err := loadJSON(userID, reqID, ctx.App.DB)
		if err != nil {
			if err.Error() == gorm.ErrRecordNotFound.Error() {
				ctx.NotFound("data might be not ready yet")
				return
			}

			ctx.InternalError(err.Error())
			return
		}

		ctx.ServeContent("result.json", data)
	}
}

func loadCSV(userID, reqID string, db *gorm.DB) ([]byte, error) {
	twitterData := new(models.Twitter)
	if err := db.Where(models.Twitter{
		UserID:    userID,
		RequestID: reqID,
		Type:      "csv",
	}).Take(twitterData).Error; err != nil {
		return []byte{}, err
	}

	return base64.StdEncoding.DecodeString(twitterData.Data)
}

func loadJSON(userID, reqID string, db *gorm.DB) ([]byte, error) {
	tweetData := new(models.Twitter)
	if err := db.Take(tweetData, models.Twitter{
		UserID:    userID,
		RequestID: reqID,
		Type:      "json",
	}).Error; err != nil {
		return []byte{}, err
	}

	decData, err := base64.StdEncoding.DecodeString(tweetData.Data)
	if err != nil {
		return []byte{}, err
	}

	//decompress data
	reader, err := gzip.NewReader(bytes.NewReader(decData))
	if err != nil {
		return []byte{}, err
	}

	return ioutil.ReadAll(reader)
}
