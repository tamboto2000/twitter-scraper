package controller

import (
	"collin/spider/util"
	"controller"
	"encoding/base64"
	"encoding/csv"
	"strconv"
	"strings"
	"twitter_scraper/app/misc/reporter"
	"twitter_scraper/app/models"
	"twitter_scraper/app/worker/counter"
	"twitter_scraper/app/worker/twitter"
)

var encKey = []byte("jZgkFPlRjFkHYPBIGvPcCj1You2A2xWA")
var nonceKey = []byte("9nQSlTeypo9Z")

func scrape(ctx *controller.Context) {
	err := ctx.Request.ParseForm()
	if err != nil {
		ctx.BadRequest(err.Error())
		return
	}

	postLimit := 0
	csvDataEnc := ctx.Request.FormValue("csvString")
	uniqueID := ctx.Request.FormValue("uniqueID")
	username := ctx.Request.FormValue("username")
	userID := ctx.Request.FormValue("userID")
	lable := ctx.Request.FormValue("lable")
	if ctx.Request.FormValue("postLimit") != "" {
		postLimit, err = strconv.Atoi(ctx.Request.FormValue("postLimit"))
		if err != nil {
			ctx.BadRequest("postLimit has invalid value")
			return
		}
	}

	if csvDataEnc == "" || uniqueID == "" || username == "" || userID == "" || lable == "" {
		ctx.BadRequest("csvString, uniqueID, userID, lable and username is need to be filled")
		return
	}

	csvBytes, err := base64.StdEncoding.DecodeString(csvDataEnc)
	if err != nil {
		ctx.BadRequest(err.Error())
		return
	}

	csvReader := csv.NewReader(strings.NewReader(string(csvBytes)))
	csvData, err := csvReader.ReadAll()
	if err != nil {
		ctx.BadRequest(err.Error())
		return
	}

	csvDataMap := util.ParseCsvArrayToMap(csvData)
	reqID := util.RandString(20)
	token := util.RandString(200)

	encToken, err := util.AESEncrypt(encKey, nonceKey, token)
	if err != nil {
		ctx.InternalError(err.Error())
		return
	}

	if err := ctx.App.DB.Create(&models.Session{
		UserID:    userID,
		RequestID: reqID,
		Token:     encToken,
	}).Error; err != nil {
		ctx.InternalError(err.Error())
		return
	}

	err = createReporterRoom(reqID, token)
	if err != nil {
		ctx.InternalError(err.Error())
		return
	}

	reporter, err := createReporter(reqID, token)
	if err != nil {
		ctx.InternalError(err.Error())
		return
	}

	worker := twitter.New()
	worker.DB = ctx.App.DB
	worker.ReqID = reqID
	worker.UserID = userID
	ctr, _ := ctx.App.Worker.Load("counter")
	worker.Counter = ctr.(*counter.Pool)
	worker.Lable = lable
	worker.CSVData = twitter.CSVData{
		UniqueIDCol:    uniqueID,
		TwrUsernameCol: username,
		Rows:           csvDataMap,
	}
	worker.Reporter = reporter
	worker.PostLimit = postLimit
	ctx.App.Worker.Add("twitter_scraper_"+reqID, worker)
	ctx.App.Worker.Run("twitter_scraper_" + reqID)

	ctx.WriteJSON(map[string]interface{}{
		"status":     "OK",
		"statusCode": 200,
		"requestID":  reqID,
		"token":      token,
	})
}

func createReporterRoom(reqID, pass string) error {
	request := util.NewRequest()
	// request.SetUrl("http://31.220.61.116:8000/createRoom")
	request.SetUrl("http://172.18.0.2:8000/createRoom")
	request.SetQuery("roomID", "twitter-"+reqID)
	request.SetQuery("roomName", "Twitter Report Channel")
	request.SetQuery("protected", "true")
	request.SetQuery("password", pass)
	err := request.Execute()
	defer request.Close()

	return err
}

func createReporter(reqID, pass string) (*reporter.Reporter, error) {
	reporter := reporter.NewReporter()
	reporter.ConnID = "twitter_master"
	reporter.RoomID = "twitter-" + reqID
	reporter.Password = pass
	reporter.Log = ctrl.Log

	return reporter, reporter.Connect()
}
