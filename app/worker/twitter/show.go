package twitter

import (
	"fmt"
	"strings"
	"sync"
	"twitter_scraper/app/misc/reporter"
	"twitter_scraper/app/worker/counter"

	"github.com/dghubble/go-twitter/twitter"
	"golang.org/x/oauth2"
)

type showWorker struct {
	reqID    string
	counter  *counter.Counter
	tweets   *sync.Map
	reporter *reporter.Reporter
	mx       *sync.Mutex
	wg       *sync.WaitGroup
	log      chan [2]string
}

func (sts *showWorker) run() {
	sts.tweets.Range(func(key, val interface{}) bool {
		sts.wg.Add(1)
		go sts.show(key.(string), val.([]twitter.Tweet))
		return true
	})
}

func (sts *showWorker) show(key string, tweets []twitter.Tweet) {
	accessToken := "AAAAAAAAAAAAAAAAAAAAADhJ5wAAAAAAqjUyp2hsjwgbYoQQyNO7I1KKQbk%3DpDfavaDSQ9DSbL9lDIXeS0ziTEh2g61N5Am8Me6Hj2MBssablE"

	configCHttp := &oauth2.Config{}
	token := &oauth2.Token{AccessToken: accessToken}
	httpClient := configCHttp.Client(oauth2.NoContext, token)

	client := twitter.NewClient(httpClient)

	for i, twt := range tweets {
		fmt.Println("lookup on ", twt.ID)

		if !strings.Contains(twt.FullText, "...") {
			continue
		}

		tweet, _, err := client.Statuses.Show(twt.ID, &twitter.StatusShowParams{
			TweetMode: "extended",
		})

		sts.counter.Decrement()

		if err != nil {
			sts.log <- [2]string{"twitter_scraper_" + sts.reqID, err.Error()}
			sts.reporter.Write("error", err.Error())
			continue
		}

		if tweet.RetweetedStatus != nil {
			retweet, _, err := client.Statuses.Show(tweet.RetweetedStatus.ID, &twitter.StatusShowParams{
				TweetMode: "extended",
			})

			sts.counter.Decrement()

			tweet.RetweetedStatus = retweet

			if err != nil {
				sts.log <- [2]string{"twitter_scraper_" + sts.reqID, err.Error()}
				sts.reporter.Write("error", err.Error())
				continue
			}
		}

		tweets[i] = *tweet
		sts.tweets.Delete(key)
		sts.tweets.Store(key, tweets)
	}

	sts.wg.Done()
}
