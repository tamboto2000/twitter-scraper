package twitter

import (
	"strings"
	"sync"
	"twitter_scraper/app/misc/reporter"
	"twitter_scraper/app/worker/counter"

	"github.com/dghubble/go-twitter/twitter"
	"golang.org/x/oauth2"
)

type Result struct {
	Fail         []Fail                     `json:"fail,omitempty"`
	ResultMap    *sync.Map                  `json:"-"`
	Result       map[string][]twitter.Tweet `json:"result"`
	ResultCount  int                        `json:"-"`
	Empty        []Empty                    `json:"empty"`
	PostCount    int                        `json:"postCount"`
	FailCount    int                        `json:"failCount"`
	SuccessCount int                        `json:"successCount"`
	EmptyCount   int                        `json:"emptyCount"`
}

const (
	FailTypeUserInvalid = "twitter_username_invalid"
	FailTypeUserError   = "twitter_user_error"
)

type Fail struct {
	Type            string `json:"type"`
	UserID          string `json:"userID,omitempty"`
	TweetID         string `json:"tweetID,omitempty"`
	TwitterUsername string `json:"twitterUsername,omitempty"`
	ErrorMessage    string `json:"errorMessage"`
}

type Empty struct {
	UserID          string `json:"userID,omitempty"`
	TwitterUsername string `json:"twitterUsername,omitempty"`
}

type CSVData struct {
	UniqueIDCol    string
	TwrUsernameCol string
	Rows           []map[string]string
}

type scrapeWorker struct {
	reqID     string
	userID    string
	lable     string
	csvData   CSVData
	reporter  *reporter.Reporter
	postLimit int
	counter   *counter.Pool
	result    Result
	wg        *sync.WaitGroup
	mx        *sync.Mutex
	log       chan [2]string
	client    *twitter.Client
}

func (wk *scrapeWorker) run() {
	accessToken := "AAAAAAAAAAAAAAAAAAAAADhJ5wAAAAAAqjUyp2hsjwgbYoQQyNO7I1KKQbk%3DpDfavaDSQ9DSbL9lDIXeS0ziTEh2g61N5Am8Me6Hj2MBssablE"

	configCHttp := &oauth2.Config{}
	token := &oauth2.Token{AccessToken: accessToken}
	httpClient := configCHttp.Client(oauth2.NoContext, token)

	wk.client = twitter.NewClient(httpClient)

	count := 100
	accounts := []map[string]string{}

	for _, acc := range wk.csvData.Rows {
		count--
		accounts = append(accounts, acc)

		if count == 0 {
			wk.wg.Add(1)
			go wk.timelineScrape(accounts)
			accounts = []map[string]string{}
			count = 100
		}
	}

	if len(accounts) > 0 {
		wk.wg.Add(1)
		go wk.timelineScrape(accounts)
	}
}

//Check if account have invalid twitter username string
func (wk *scrapeWorker) checkAndFilterUsername(uniqueID, username string) string {
	username = filter(username)
	if username == "" {
		wk.mx.Lock()
		wk.reporter.Write("error", "User "+uniqueID+" has invalid twitter username")

		wk.result.Fail = append(wk.result.Fail, Fail{
			Type:         FailTypeUserInvalid,
			UserID:       uniqueID,
			ErrorMessage: "User " + uniqueID + " has invalid twitter username",
		})

		wk.result.FailCount++

		wk.mx.Unlock()
		return ""
	}

	return username
}

func (wk *scrapeWorker) storeResult(uniqueID string, tweets []twitter.Tweet) {
	wk.mx.Lock()
	wk.result.ResultCount++
	wk.result.SuccessCount++
	wk.result.PostCount += len(tweets)

	wk.result.ResultMap.Store(uniqueID, tweets)

	wk.mx.Unlock()
}

func (wk *scrapeWorker) timelineScrape(users []map[string]string) {
	for _, user := range users {
		timelineCounter, _ := wk.counter.Load("timeline")
		uniqueID := user[wk.csvData.UniqueIDCol]
		username := wk.checkAndFilterUsername(uniqueID, user[wk.csvData.TwrUsernameCol])
		if username == "" {
			continue
		}

		wk.reporter.Write("info", "Scraping user "+uniqueID)

		userTweets := []twitter.Tweet{}

		includeRetweets := true
		tweets, _, err := wk.client.Timelines.UserTimeline(&twitter.UserTimelineParams{
			Count:           200,
			IncludeRetweets: &includeRetweets,
			ScreenName:      username,
			TweetMode:       "extended",
		})

		timelineCounter.Decrement()

		if err != nil {
			wk.mx.Lock()

			wk.result.Fail = append(wk.result.Fail, Fail{
				Type:            FailTypeUserError,
				UserID:          user[uniqueID],
				TwitterUsername: username,
				ErrorMessage:    user[uniqueID] + ": " + err.Error(),
			})

			wk.result.FailCount++

			wk.mx.Unlock()
			wk.reporter.Write("error", "Error when sraping user "+user[uniqueID]+": "+err.Error())
			continue
		}

		userTweets = append(userTweets, tweets...)
		if wk.postLimit > 0 && len(userTweets) >= wk.postLimit {
			wk.storeResult(uniqueID, userTweets[:wk.postLimit])
			wk.reporter.Write("info", "User "+uniqueID+" successfully scraped")
			continue
		}

		var cursor int64
		if len(tweets) == 200 {
			cursor = tweets[199].ID
		}

		for cursor != 0 {
			tweets, _, err := wk.client.Timelines.UserTimeline(&twitter.UserTimelineParams{
				Count:           200,
				IncludeRetweets: &includeRetweets,
				MaxID:           int64(cursor),
				ScreenName:      username,
				TweetMode:       "extended",
			})

			timelineCounter.Decrement()
			if err != nil {
				wk.reporter.Write("error", "Error while cursoring on user "+user[uniqueID]+": "+err.Error())
			}

			userTweets = append(userTweets, tweets...)
			if wk.postLimit > 0 && len(userTweets) >= wk.postLimit {
				userTweets = userTweets[:wk.postLimit]
				break
			}

			if len(tweets) == 200 {
				cursor = tweets[199].ID
			} else {
				break
			}
		}

		wk.storeResult(uniqueID, userTweets)
		wk.reporter.Write("info", "User "+uniqueID+" successfully scraped")
	}

	wk.wg.Done()
}

func filter(r string) string {
	if strings.Contains(r, " ") {
		return ""
	}

	if r == "0" {
		return ""
	}

	if r == "-" {
		return ""
	}

	if strings.Contains(r, "@yahoo") {
		return ""
	}

	if strings.Contains(r, "@gmail") {
		return ""
	}

	if strings.Contains(r, "@rocket") {
		return ""
	}

	if strings.Contains(r, "@") {
		r = strings.Replace(r, "@", "", -1)

		return r
	}

	if strings.Contains(r, "https://") {
		if !strings.Contains(r, "twitter.com") {
			return ""
		}

		stg1 := strings.Split(r, "/")

		if stg1[len(stg1)-1] != "" {
			if strings.Contains(stg1[len(stg1)-1], "?") || strings.Contains(stg1[len(stg1)-1], "=") {
				return stg1[len(stg1)-2]
			}

			return stg1[len(stg1)-1]
		} else {
			return stg1[len(stg1)-2]
		}
	}

	if strings.Contains(r, "/") {
		stg1 := strings.Split(r, "/")

		if stg1[len(stg1)-1] != "" {
			return stg1[len(stg1)-1]
		} else {
			if strings.Contains(stg1[len(stg1)-2], "twitter.com") {
				return ""
			}

			return stg1[len(stg1)-2]
		}
	}

	return r
}
