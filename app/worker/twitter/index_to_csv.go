package twitter

import (
	"bytes"
	"encoding/base64"
	"encoding/csv"
	"fmt"
	"strconv"
	"sync"
	"twitter_scraper/app/misc/reporter"
	"twitter_scraper/app/models"
	"ziper"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/jinzhu/gorm"
)

//indexWorker is a singleton for indexing functions
type indexToCSVWorker struct {
	db       *gorm.DB
	data     *Result
	wg       *sync.WaitGroup
	csvMutex *sync.Mutex
	reporter *reporter.Reporter
	log      chan [2]string
	userID   string
	lable    string
	reqID    string
	err      error
}

func (wk *indexToCSVWorker) run() {
	tweetHeader := []string{"user", "post_id", "caption", "like", "retweet_count", "created_at"}
	tweetMediaHeader := []string{"post_id", "url", "type"}
	locationHeader := []string{"post_id", "long", "lat"}

	retweetHeader := []string{"user", "post_id", "retweet_post_id", "retweet_owner", "caption", "like", "retweet_count", "created_at"}
	retweetMediaHeader := []string{"post_id", "url", "type"}
	retweetLocationHeader := []string{"post_id", "long", "lat"}

	tweetData := [][]string{tweetHeader}
	tweetMediaData := [][]string{tweetMediaHeader}
	tweetLocationData := [][]string{locationHeader}

	retweetData := [][]string{retweetHeader}
	retweetMediaData := [][]string{retweetMediaHeader}
	retweetLocationData := [][]string{retweetLocationHeader}

	csvMap := map[string][][]string{
		"tweet.csv":            tweetData,
		"tweet_media.csv":      tweetMediaData,
		"tweet_location.csv":   tweetLocationData,
		"retweet.csv":          retweetData,
		"retweet_media.csv":    retweetMediaData,
		"retweet_location.csv": retweetLocationData,
	}

	indexer := indexer{
		csvMap: csvMap,
		wg:     new(sync.WaitGroup),
		mx:     new(sync.Mutex),
	}
	wk.data.ResultMap.Range(indexer.index)
	indexer.wg.Wait()

	zipBytes, err := compressCSVToZIP(indexer.csvMap)
	if err != nil {
		wk.reporter.Write("fatal_error", err.Error())
		wk.log <- [2]string{"twitter_scraper_" + wk.reqID, err.Error()}
		wk.wg.Done()
	}

	//Encode zip to base64 string
	encodedZIP := base64.StdEncoding.EncodeToString(zipBytes)

	//Store to db
	if err := wk.db.Create(&models.Twitter{
		Type:      "csv",
		UserID:    wk.userID,
		RequestID: wk.reqID,
		Data:      encodedZIP,
	}).Error; err != nil {
		wk.reporter.Write("fatal_error", err.Error())
		wk.log <- [2]string{"twitter_scraper_" + wk.reqID, err.Error()}
		wk.wg.Done()
	}
}

func compressCSVToZIP(tweets map[string][][]string) ([]byte, error) {
	//create zip writer
	zipWriter := ziper.NewZiper()

	//Convert arrays to csv string
	for key, data := range tweets {
		str := ""
		buff := bytes.NewBufferString(str)
		writer := csv.NewWriter(buff)
		err := writer.WriteAll(data)
		if err != nil {
			zipWriter.Close()
			return []byte{}, err
		}

		zipWriter.Add(key, buff.String())
	}

	err := zipWriter.Close()
	if err != nil {
		return []byte{}, err
	}

	return zipWriter.Bytes(), nil
}

type indexer struct {
	csvMap map[string][][]string
	mx     *sync.Mutex
	wg     *sync.WaitGroup
}

func (idx *indexer) index(key, tweets interface{}) bool {
	idx.wg.Add(1)
	go func() {
		for _, tweet := range tweets.([]twitter.Tweet) {
			idx.mx.Lock()
			idx.csvMap["tweet.csv"] = append(idx.csvMap["tweet.csv"], []string{
				key.(string),
				tweet.IDStr,
				tweet.FullText,
				strconv.Itoa(tweet.FavoriteCount),
				strconv.Itoa(tweet.RetweetCount),
				tweet.CreatedAt,
			})

			if len(tweet.Entities.Media) != 0 {
				for _, media := range tweet.Entities.Media {
					idx.csvMap["tweet_media.csv"] = append(idx.csvMap["tweet_media.csv"], []string{
						tweet.IDStr,
						media.ExpandedURL,
						media.Type,
					})
				}
			}

			if tweet.Coordinates != nil {
				long := fmt.Sprintf("%g", tweet.Coordinates.Coordinates[0])
				lat := fmt.Sprintf("%g", tweet.Coordinates.Coordinates[1])

				idx.csvMap["tweet_location.csv"] = append(idx.csvMap["tweet_location.csv"], []string{
					tweet.IDStr,
					long,
					lat,
				})
			}

			postID := tweet.IDStr

			if tweet.RetweetedStatus != nil {
				tweet := tweet.RetweetedStatus

				idx.csvMap["retweet.csv"] = append(idx.csvMap["retweet.csv"], []string{
					key.(string),
					postID,
					tweet.IDStr,
					tweet.User.IDStr,
					tweet.FullText,
					strconv.Itoa(tweet.FavoriteCount),
					strconv.Itoa(tweet.RetweetCount),
					tweet.CreatedAt,
				})

				if len(tweet.Entities.Media) != 0 {
					for _, media := range tweet.Entities.Media {
						idx.csvMap["retweet_media.csv"] = append(idx.csvMap["retweet_media.csv"], []string{
							tweet.IDStr,
							media.ExpandedURL,
							media.Type,
						})
					}
				}

				if tweet.Coordinates != nil {
					long := fmt.Sprintf("%g", tweet.Coordinates.Coordinates[0])
					lat := fmt.Sprintf("%g", tweet.Coordinates.Coordinates[1])

					idx.csvMap["retweet_location.csv"] = append(idx.csvMap["retweet_location.csv"], []string{
						tweet.IDStr,
						long,
						lat,
					})
				}
			}
			idx.mx.Unlock()
		}

		idx.wg.Done()
	}()

	return true
}
