package twitter

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"encoding/json"
	"sync"
	"twitter_scraper/app/misc/reporter"
	"twitter_scraper/app/models"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/jinzhu/gorm"
)

type indexToJSONWorker struct {
	db       *gorm.DB
	data     *Result
	wg       *sync.WaitGroup
	csvMutex *sync.Mutex
	reporter *reporter.Reporter
	log      chan [2]string
	userID   string
	lable    string
	reqID    string
	err      error
}

func (wk *indexToJSONWorker) run() {
	resJSON, err := wk.normalize()
	if err != nil {
		wk.log <- [2]string{"twitter_scraper_" + wk.reqID, err.Error()}
		wk.reporter.Write("fatal_error", err.Error())
	}

	encData, err := wk.compressAndEncode(resJSON)
	if err != nil {
		wk.log <- [2]string{"twitter_scraper_" + wk.reqID, err.Error()}
		wk.reporter.Write("fatal_error", err.Error())
	}

	if err := wk.db.Create(&models.Twitter{
		UserID:    wk.userID,
		RequestID: wk.reqID,
		Type:      "json",
		Data:      encData,
	}).Error; err != nil {
		wk.log <- [2]string{"twitter_scraper_" + wk.reqID, err.Error()}
		wk.reporter.Write("fatal_error", err.Error())
	}
}

func (wk *indexToJSONWorker) compressAndEncode(data []byte) (string, error) {
	buffer := bytes.NewBuffer([]byte{})
	gzWriter, err := gzip.NewWriterLevel(buffer, gzip.BestCompression)
	if err != nil {
		return "", err
	}

	if _, err := gzWriter.Write(data); err != nil {
		return "", err
	}

	if err := gzWriter.Close(); err != nil {
		return "", err
	}

	enc := base64.StdEncoding.EncodeToString(buffer.Bytes())
	return enc, nil
}

func (wk *indexToJSONWorker) normalize() ([]byte, error) {
	wk.data.ResultMap.Range(func(key, val interface{}) bool {
		wk.data.Result[key.(string)] = val.([]twitter.Tweet)

		return true
	})

	return json.Marshal(wk.data)
}
