package twitter

import (
	"sync"
	"twitter_scraper/app/misc/reporter"
	"twitter_scraper/app/models"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/jinzhu/gorm"
)

type indexToTableWorker struct {
	db       *gorm.DB
	data     *Result
	wg       *sync.WaitGroup
	csvMutex *sync.Mutex
	reporter *reporter.Reporter
	log      chan [2]string
	userID   string
	lable    string
	reqID    string
	err      error
}

func (wk *indexToTableWorker) run() {
	wk.data.ResultMap.Range(func(key, val interface{}) bool {
		for _, tweet := range val.([]twitter.Tweet) {
			wk.storeTweet(key.(string), &tweet)
			wk.storeTweetMedia(&tweet)
			wk.storeTweetLocation(&tweet)
			wk.storeRetweet(key.(string), &tweet)
			wk.storeRetweetMedia(&tweet)
			wk.storeRetweetLocation(&tweet)
		}

		return true
	})
}

func (wk *indexToTableWorker) storeTweet(userID string, tweet *twitter.Tweet) {
	if err := wk.db.Create(&models.Tweet{
		User:         userID,
		PostID:       tweet.IDStr,
		Caption:      tweet.FullText,
		Like:         tweet.FavoriteCount,
		RetweetCount: tweet.RetweetCount,
	}).Error; err != nil {
		wk.reporter.Write("fatal_error", err.Error())
		wk.log <- [2]string{"twitter_scraper_" + wk.reqID, err.Error()}
	}
}

func (wk *indexToTableWorker) storeTweetMedia(tweet *twitter.Tweet) {
	if tweet.Entities != nil {
		if len(tweet.Entities.Media) != 0 {
			for _, media := range tweet.Entities.Media {
				if err := wk.db.Create(&models.TweetMedia{
					PostID: tweet.IDStr,
					URL:    media.ExpandedURL,
					Type:   media.Type,
				}).Error; err != nil {
					wk.reporter.Write("fatal_error", err.Error())
					wk.log <- [2]string{"twitter_scraper_" + wk.reqID, err.Error()}
				}
			}
		}
	}
}

func (wk *indexToTableWorker) storeTweetLocation(tweet *twitter.Tweet) {
	if tweet.Coordinates != nil {
		if err := wk.db.Create(&models.TweetLocation{
			PostID: tweet.IDStr,
			Long:   tweet.Coordinates.Coordinates[0],
			Lat:    tweet.Coordinates.Coordinates[1],
		}).Error; err != nil {
			wk.reporter.Write("fatal_error", err.Error())
			wk.log <- [2]string{"twitter_scraper_" + wk.reqID, err.Error()}
		}
	}
}

func (wk *indexToTableWorker) storeRetweet(userID string, tweet *twitter.Tweet) {
	if tweet.RetweetedStatus != nil {
		if err := wk.db.Create(&models.Retweet{
			User:          userID,
			PostID:        tweet.IDStr,
			RetweetPostID: tweet.RetweetedStatus.IDStr,
			RetweetOwner:  tweet.RetweetedStatus.User.ScreenName,
			Caption:       tweet.FullText,
			Like:          tweet.FavoriteCount,
			RetweetCount:  tweet.RetweetCount,
		}).Error; err != nil {
			wk.reporter.Write("fatal_error", err.Error())
			wk.log <- [2]string{"twitter_scraper_" + wk.reqID, err.Error()}
		}
	}
}

func (wk *indexToTableWorker) storeRetweetMedia(tweet *twitter.Tweet) {
	if tweet.RetweetedStatus != nil {
		retweet := tweet.RetweetedStatus
		if retweet.Entities != nil {
			if len(retweet.Entities.Media) != 0 {
				for _, media := range retweet.Entities.Media {
					if err := wk.db.Create(&models.RetweetMedia{
						PostID: tweet.IDStr,
						URL:    media.ExpandedURL,
						Type:   media.Type,
					}).Error; err != nil {
						wk.reporter.Write("fatal_error", err.Error())
						wk.log <- [2]string{"twitter_scraper_" + wk.reqID, err.Error()}
					}
				}
			}
		}
	}
}

func (wk *indexToTableWorker) storeRetweetLocation(tweet *twitter.Tweet) {
	if tweet.RetweetedStatus != nil {
		retweet := tweet.RetweetedStatus
		if retweet.Coordinates != nil {
			if err := wk.db.Create(&models.RetweetLocation{
				PostID: retweet.IDStr,
				Long:   retweet.Coordinates.Coordinates[0],
				Lat:    retweet.Coordinates.Coordinates[1],
			}).Error; err != nil {
				wk.reporter.Write("fatal_error", err.Error())
				wk.log <- [2]string{"twitter_scraper_" + wk.reqID, err.Error()}
			}
		}
	}
}
