package twitter

import (
	"sync"
	"twitter_scraper/app/misc/reporter"
	"twitter_scraper/app/worker/counter"

	"github.com/dghubble/go-twitter/twitter"

	"github.com/jinzhu/gorm"
)

type Twitter struct {
	DB               *gorm.DB
	ReqID            string
	UserID           string
	Counter          *counter.Pool
	Lable            string
	CSVData          CSVData
	Reporter         *reporter.Reporter
	PostLimit        int
	scrapeWorker     *scrapeWorker
	indexToCSVWorker *indexToCSVWorker
	showWorker       *showWorker
	exit             chan bool
	log              chan [2]string
}

func New() *Twitter {
	return &Twitter{
		exit: make(chan bool),
		log:  make(chan [2]string),
	}
}

func (twr *Twitter) Run() error {
	go func() {
		twr.runScrapeWorker()
		twr.scrapeWorker.wg.Wait()
		twr.Reporter.Write("info", "Stage 1 complete, moving to stage 2, you will not able to see any log unless if it error or fatal_error")

		twr.runShowWorker()
		twr.showWorker.wg.Wait()
		twr.Reporter.Write("info", "Stage 2 complete, start indexing...")

		wg := new(sync.WaitGroup)
		wg.Add(3)

		go func() { twr.runIndexToCSVWorker(); wg.Done() }()
		go func() { twr.runIndexToJSONWorker(); wg.Done() }()
		go func() { twr.runIndexToTableWorker(); wg.Done() }()

		wg.Wait()
		twr.Reporter.Write("finish", "finish")

		twr.exit <- true
	}()

	return nil
}

func (twr *Twitter) Wait() {
	<-twr.exit
}

func (twr *Twitter) ReadLog() chan [2]string {
	return twr.log
}

func (twr *Twitter) TerminateOnFinish() bool {
	return true
}

func (twr *Twitter) CleanUp() error {
	return nil
}

func (twr *Twitter) runScrapeWorker() {
	scraper := &scrapeWorker{
		reqID:     twr.ReqID,
		userID:    twr.ReqID,
		lable:     twr.Lable,
		csvData:   twr.CSVData,
		reporter:  twr.Reporter,
		postLimit: twr.PostLimit,
		counter:   twr.Counter,
		wg:        new(sync.WaitGroup),
		mx:        new(sync.Mutex),
		log:       twr.log,
	}

	twr.scrapeWorker = scraper
	scraper.result.ResultMap = new(sync.Map)
	scraper.result.Result = make(map[string][]twitter.Tweet)

	scraper.run()
}

func (twr *Twitter) runShowWorker() {
	counter, _ := twr.Counter.Load("show")
	worker := &showWorker{
		reqID:    twr.ReqID,
		reporter: twr.Reporter,
		tweets:   twr.scrapeWorker.result.ResultMap,
		mx:       new(sync.Mutex),
		wg:       new(sync.WaitGroup),
		counter:  counter,
		log:      twr.log,
	}

	twr.showWorker = worker

	worker.run()
}

func (twr *Twitter) runIndexToCSVWorker() {
	worker := &indexToCSVWorker{
		db:       twr.DB,
		data:     &twr.scrapeWorker.result,
		wg:       new(sync.WaitGroup),
		csvMutex: new(sync.Mutex),
		reporter: twr.Reporter,
		log:      twr.log,
		userID:   twr.UserID,
		lable:    twr.Lable,
		reqID:    twr.ReqID,
	}

	worker.run()
}

func (twr *Twitter) runIndexToJSONWorker() {
	worker := &indexToJSONWorker{
		db:       twr.DB,
		data:     &twr.scrapeWorker.result,
		wg:       new(sync.WaitGroup),
		csvMutex: new(sync.Mutex),
		reporter: twr.Reporter,
		log:      twr.log,
		userID:   twr.UserID,
		lable:    twr.Lable,
		reqID:    twr.ReqID,
	}

	worker.run()
}

func (twr *Twitter) runIndexToTableWorker() {
	worker := &indexToTableWorker{
		db:       twr.DB,
		data:     &twr.scrapeWorker.result,
		wg:       new(sync.WaitGroup),
		csvMutex: new(sync.Mutex),
		reporter: twr.Reporter,
		log:      twr.log,
		userID:   twr.UserID,
		lable:    twr.Lable,
		reqID:    twr.ReqID,
	}

	worker.run()
}
