package counter

import (
	"fmt"
	"sync"
	"time"
)

type Counter struct {
	count        int //keep the original counter
	counter      int //used counter. if 0 counter = count
	mx           *sync.Mutex
	onZero       func() //recommended to not use concurrency
	refreshEvery int
}

type Config struct {
	Counter      int
	OnZero       func()
	RefreshEvery int
}

type Pool struct {
	counters *sync.Map
}

func New() *Pool {
	pool := &Pool{
		counters: new(sync.Map),
	}

	pool.NewCounter("timeline", Config{
		Counter:      1500,
		OnZero:       func() { fmt.Println("counter sleep"); time.Sleep(900 * time.Second) },
		RefreshEvery: 900,
	})

	pool.NewCounter("show", Config{
		Counter:      900,
		OnZero:       func() { fmt.Println("counter sleep"); time.Sleep(900 * time.Second) },
		RefreshEvery: 900,
	})

	return pool
}

func (p *Pool) NewCounter(name string, conf Config) {
	counter := newCounter(conf)
	p.counters.Store(name, counter)
}

func (p *Pool) Load(name string) (*Counter, bool) {
	counter, ok := p.counters.Load(name)

	return counter.(*Counter), ok
}

func (p *Pool) Run() error {
	return nil
}

func (p *Pool) Wait() {
	return
}

func (p *Pool) ReadLog() chan [2]string {
	return make(chan [2]string)
}

func (p *Pool) TerminateOnFinish() bool {
	return false
}

func (p *Pool) CleanUp() error {
	return nil
}

func (p *Pool) GetData() interface{} {
	return ""
}

func (p *Pool) StreamData() chan interface{} {
	return make(chan interface{})
}

func (p *Pool) Command(val interface{}) {}

//OnZero implement func() for condition c.counter = 0
func (c *Counter) OnZero(fn func()) {
	c.onZero = fn
}

func (c *Counter) SetCounter(count int) {
	c.count = count
	c.counter = count
}

func (c *Counter) Decrement() {
	c.mx.Lock()
	c.counter--
	if c.counter == 0 {
		if c.onZero != nil {
			c.onZero()
		}

		c.counter = c.count
	}
	c.mx.Unlock()
}

func (c *Counter) Increment() {
	c.mx.Lock()
	c.counter++
	c.mx.Unlock()
}

func (c *Counter) timedRefresh() {
	ticker := time.NewTicker(time.Duration(c.refreshEvery * int(time.Second)))

	for {
		select {
		case <-ticker.C:
			c.mx.Lock()
			c.counter = c.count
			c.mx.Unlock()
		}
	}
}

func newCounter(conf Config) *Counter {
	counter := &Counter{
		count:        conf.Counter,
		counter:      conf.Counter,
		mx:           new(sync.Mutex),
		onZero:       conf.OnZero,
		refreshEvery: conf.RefreshEvery,
	}

	if conf.RefreshEvery > 0 {
		go counter.timedRefresh()
	}

	return counter
}
