package worker

import (
	"twitter_scraper/app/worker/counter"
	"worker"
)

var pool = worker.NewPool()

//Pool return pool for workers, register workers in here, or register it somewhere
//if the worker need dynamic data
func Pool() *worker.Pool {
	pool.Add("counter", counter.New())

	return pool
}
