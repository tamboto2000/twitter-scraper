package main

import (
	"twitter_scraper/app/models"

	"github.com/jinzhu/gorm"
)

func migrate(db *gorm.DB) {
	db.AutoMigrate(
		models.Session{},
		models.Tweet{},
		models.Twitter{},
		models.TweetMedia{},
		models.TweetLocation{},
		models.Retweet{},
		models.RetweetMedia{},
		models.RetweetLocation{},
	)
}

func reMigrate(db *gorm.DB) {
	if err := db.DropTable(
		models.Session{},
		models.Tweet{},
		models.Twitter{},
		models.TweetMedia{},
		models.TweetLocation{},
		models.Retweet{},
		models.RetweetMedia{},
		models.RetweetLocation{},
	).Error; err != nil {
		panic(err.Error())
	}

	if err := db.CreateTable(
		models.Session{},
		models.Tweet{},
		models.Twitter{},
		models.TweetMedia{},
		models.TweetLocation{},
		models.Retweet{},
		models.RetweetMedia{},
		models.RetweetLocation{},
	).Error; err != nil {
		panic(err.Error())
	}
}
