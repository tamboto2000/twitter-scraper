package router

import (
	"net/http"

	"controller"

	"github.com/gorilla/mux"
)

type MiddleFunc func(w http.ResponseWriter, r *http.Request) bool

type Router struct {
	Router   *mux.Router
	ty       string
	rootPath string
	root     *mux.Router
	lastPath string
}

func NewRouter() *Router {
	router := mux.NewRouter()
	return &Router{
		Router: router,
		root:   router,
	}
}

func (r *Router) NewSubrouter(path string) *Router {
	sub := r.Router.PathPrefix(path).Subrouter()
	return &Router{
		Router:   sub,
		root:     r.root,
		ty:       "subrouter",
		rootPath: path,
	}
}

func (r *Router) AddFunc(path string, ctrlFunc controller.HTTPFunc, method ...string) *Router {
	r.Router.HandleFunc(path, ctrlFunc).Methods(method...)
	r.lastPath = path

	return r
}

func (r *Router) Middleware(mdl MiddleFunc) {
	path := r.lastPath
	if r.ty == "subrouter" {
		path = r.rootPath + r.lastPath
	}

	r.root.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path == path {
				if mdl(w, r) {
					next.ServeHTTP(w, r)
				}
			} else {
				next.ServeHTTP(w, r)
			}
		})
	})
}

func (r *Router) ExportRouter() *mux.Router {
	return r.root
}
