package ziper

import (
	"archive/zip"
	"bytes"
)

type Ziper struct {
	buff   *bytes.Buffer
	writer *zip.Writer
	Error  error
}

func NewZiper() *Ziper {
	buff := new(bytes.Buffer)
	writer := zip.NewWriter(buff)

	return &Ziper{
		buff:   buff,
		writer: writer,
	}
}

func (ziper *Ziper) Close() error {
	return ziper.writer.Close()
}

func (ziper *Ziper) Bytes() []byte {
	return ziper.buff.Bytes()
}

func (ziper *Ziper) Add(fname, data string) {
	f, err := ziper.writer.Create(fname)
	if err != nil {
		ziper.Error = err

		return
	}

	_, err = f.Write([]byte(data))
	if err != nil {
		ziper.Error = err

		return
	}
}
